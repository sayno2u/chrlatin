// АБВГДЕЁЖЗИЙКЛМНОӨПРСТУҮФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмноөпрстуүфхцчшщъыьэюя
var mnDict = {
	"А": "A",
	"Б": "B",
	"В": "V",
	"Г": "G",
	"Д": "D",
	"Е": "Ye",
	"Ё": "Yo",
	"Ж": "J",
	"З": "Z",
	"И": "I",
	"Й": "I",
	"К": "K",
	"Л": "L",
	"М": "M",
	"Н": "N",
	"О": "O",
	"Ө": "Ö",
	"П": "P",
	"Р": "R",
	"С": "S",
	"Т": "T",
	"У": "U",
	"Ү": "Ü",
	"Ф": "F",
	"Х": "H",
	"Ц": "C",
	"Ч": "Q",
	"Ш": "X",
	"Щ": "X",
	"Ъ": "",
	"Ы": "II",
	"Ь": "I",
	"Э": "E",
	"Ю": "Yu",
	"Я": "Ya",
	"а": "a",
	"б": "b",
	"в": "v",
	"г": "g",
	"д": "d",
	"е": "ye",
	"ё": "yo",
	"ж": "j",
	"з": "z",
	"и": "i",
	"й": "i",
	"к": "k",
	"л": "l",
	"м": "m",
	"н": "n",
	"о": "o",
	"ө": "ö",
	"п": "p",
	"р": "r",
	"с": "s",
	"т": "t",
	"у": "u",
	"ү": "ü",
	"ф": "f",
	"х": "h",
	"ц": "c",
	"ч": "q",
	"ш": "x",
	"щ": "x",
	"ъ": "",
	"ы": "ii",
	"ь": "i",
	"э": "e",
	"ю": "yu",
	"я": "ya"
};
var webElement = ["head", "body"];
var loadCheckInterval = 1;
var loadCheck;
var toggleLatinMN = function(e) {
	if (!e) {
		document.location.reload();
		return;
	}
	transliterate();
};

var convertToLatin = function (item) {
	for (var mnKey in mnDict) {
		if (mnDict.hasOwnProperty(mnKey)) {
			var re = new RegExp(mnKey, "g");
			item = item.replace(re, mnDict[mnKey])
		}
	};
	return item;
}

function transliterate() {
	for (var elemIndex = 0; elemIndex < webElement.length; elemIndex++) {
		var nodes = document.getElementsByTagName(webElement[elemIndex]);
		for (var i = 0; i < nodes.length; i++) {
			nodeHTML = nodes[i].innerHTML;
			var output = [];
			var txtArray = [];
			for (var j = 0; j < nodeHTML.length; j++) {
				if (nodeHTML[j] !== "<") {
					txtArray = txtArray + nodeHTML[j];
				} else {
					if (txtArray.length !== 0) {
						output = output + convertToLatin(txtArray);
					}
					txtArray = [];
					for (var k = j + 1; k < nodeHTML.length; k++) {
						if (nodeHTML[k] === ">") {
							output = output + nodeHTML.slice(j, k + 1)
							j = k;
							break;
						}
					}
				}
			}
			if (txtArray.length === 0) {
				nodes[i].innerHTML = output;
			} else {
				nodes[i].innerHTML = output + convertToLatin(txtArray);
			}
		}
	}
}

var showPage = function(e) {
	if (e) document.documentElement.style.display = "";
	else document.documentElement.style.display = "false"
};
var doLoadCheck = function() {
	loadCheck = window.setInterval(function() {
		if (document.readyState === "complete") {
			transliterate();
			window.clearInterval(loadCheck);
			return
		}
		transliterate();
	}, loadCheckInterval)
}