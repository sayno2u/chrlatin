/**
 * Gulp build tasks for the ChrLatin chrome extension.
 * Author: Sancho
 * After installing gulp, run the command `gulp` in the repo root directory
 */
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var babel = require('gulp-babel');

// gulp default task
gulp.task('default', ['uglify', 'move-manifest', 'move-css', 'copy-image']);

// run gulp watch task `gulp watch`
gulp.task('watch', () => {
	gulp.watch(['*.js', '*.css', 'icons/*.png'], ['default']);
});

gulp.task('uglify', () => {
	return gulp.src(['contentscripts.js', 'background.js'])
		.pipe(babel())
		.pipe(uglify())
		.pipe(gulp.dest('dist'));
});

gulp.task('move-manifest', () => {
	return gulp.src(['manifest.json'])
		.pipe(gulp.dest('dist'));
});

gulp.task('move-css', () => {
	return gulp.src(['contentscripts.css'])
		.pipe(gulp.dest('dist'));
})

gulp.task('copy-image', () => {
	return gulp.src(['icons/*.png'])
		.pipe(gulp.dest('dist/icons'));
});