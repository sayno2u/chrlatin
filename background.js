var tabArray = [];
var checkToggle = function (e) {
	"use strict";
	if (!tabArray[e.id]) {
		tabArray[e.id] = {};
	}
	var t = tabArray[e.id];
	t.latinText = !(t.latinText === true ? true : false);
	chrome.tabs.executeScript(e.id, {
		code: "toggleLatinMN(" + t.latinText.toString() + ")"
	});
	if (t.latinText) {
		chrome.browserAction.setBadgeText({
			text: "on",
			tabId: e.id
		});
	} else {
		chrome.browserAction.setBadgeText({
			text: "",
			tabId: e.id
		});
	}
};
var mnuNewTab = chrome.contextMenus.create({
	title: "Latinaar xine conhond neene üü",
	contexts: ["link"],
	onclick: function(e, t) {
		latinTextTab(e.linkUrl, t);
	}
});
var latinTextTab = function(e, t) {
	chrome.tabs.create({
		url: e,
		active: true,
		openerTabId: t.id
	}, function(e) {
		tabArray[e.id] = {
			latinText: true
		};
	});
};
chrome.browserAction.onClicked.addListener(function(e) {
	checkToggle(e);
});
chrome.tabs.onUpdated.addListener(function(e, t, n) {
	if (!tabArray[e] || tabArray[e].latinText !== true) {
		return;
	}
	chrome.browserAction.setBadgeText({
		text: "on",
		tabId: n.id
	});
	switch (t.status) {
		case "loading":
			chrome.tabs.executeScript(n.id, {
				code: "doLoadCheck()",
				runAt: "document_start"
			});
			break;
		case "complete":
			chrome.tabs.executeScript(n.id, {
				code: "toggleLatinMN(" + tabArray[e].latinText.toString() + ")"
			});
			chrome.tabs.executeScript(n.id, {
				code: "showPage(true)"
			});
			break;
	}
})